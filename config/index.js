const dotenv = require('dotenv').config();

if( process.env.UNIDAD_NEGOCIO == 1 ){
  // INBI SCHOOL
  console.log('/***************** INBI SCHOOL ******************/')
  module.exports = {
    ENV: process.env.ENV,
    PORT: process.env.PORT || 3001,
    USUARIO_CORREO:  'staff.inbischool@gmail.com',
    PASSWORD_CORREO: 'sgjywlyabjawvlzv',
    ESCUELA:         'INBI SCHOOL',
    LOGO:            'https://inbi.mx/public/images/logo_blanco.png',
    RUTA_PAGO:       'https://qa.inbi.mx/procesarpago.php',
    UNIDAD_NEGOCIO:  '1',
    FACEBOOK:        'https://www.facebook.com/INBImx/',
    INSTAGRAM:       'https://www.instagram.com/inbimx/?hl=es-la',
    ICONO_FACEBOOK:  'https://inbi.mx/public/images/ico_facebook.jpg',
    ICONO_INSTAGRAM: 'https://inbi.mx/public/images/ico_instagram.jpg',
    URL_ACTIVA:      'https://inbi.mx/activarcuenta/#/',
    RUTA_ARCHIVOS:   'https://lms.inbi.mx/',
    RUTA_MANUAL_CERTIFICACION: 'https://lms.inbi.mx/manuales/certificacion_usuario_INBI.pdf',
    RUTA_EXAMEN_CERTIFICACION: 'https://www.metritests.com/metrica/',
    RUTA_EXCI:       'https://lms.inbi.mx/exci/exci.php?id=',
    URL_HORARIOS     : 'https://inbi.mx/public/images/inbi_horarios_2023.png',
    urlTeens         : '',
    URL_PAGOS        : '',
    TOKEN            : 'f7RGvnRCq0G2pW9cJkrY6O:APA91bFHHrPpvE7G9wfYdjDH54boS1Rjy_Aeq-FaD7cdPeUDGbaIIsSlByq4nif23fFKpb-aSoLbIRS_1SM1gVcw5N4430OjmXTDoyASq-AvME4-Oa2rk4ySw0awh5NAA2Nx6Y1-33kJ',
    TOKENDESARROLLO  : 'cMK81le8_UCfvLUtXTBsRE:APA91bGkw0JKJ2Gvy6iUqXyTmDtrKk8Ncpg63byk7EGx6Uj19mIrU3oj2ZWN6oSPntIlQokg_G1gBNtwKQvOJVufDxnSdYXJZ96-LVNudv87e5o95ry4Bh2SPeCO0Nt67hhA41pfX-Zz',
    TOKENARMANDO     : 'd6FwKI_ERaiiI9uQ1pJmjq:APA91bE0vhRYVxoY_ZSvyt49irOKrmgruWVWkO4eCWcaD4xQF4_jqn8FwxbgnphihSq0N6bVzi49SNrGaKWIsLgFpizB3vXbxqgc-mRBpKpu-Ib705cUWKwjKNov1j5ePKtnCIQaJoOm'

  }
}else{
  // FAST ENGLISH
  console.log('/**************** FAST ENGLISH ******************/')
  module.exports = {
    ENV: process.env.ENV,
    PORT: process.env.PORT || 3001,
    USUARIO_CORREO:  'staff.fastenglish@gmail.com',
    PASSWORD_CORREO: 'jzxmdixwyauwxfbf',
    ESCUELA:         'FAST ENGLISH',
    LOGO:            'https://fastenglish.com.mx/public/images/logo.png',
    RUTA_PAGO:       'https://qa.fastenglish.com.mx/procesarpago.php',
    UNIDAD_NEGOCIO:  '2',
    FACEBOOK:        'https://www.facebook.com/fastenglishmty',
    INSTAGRAM:       'https://instagram.com/fastenglishschool?utm_medium=copy_link',
    ICONO_FACEBOOK:  'https://fastenglish.com.mx/public/images/ico_facebook.jpg',
    ICONO_INSTAGRAM: 'https://fastenglish.com.mx/public/images/ico_instagram.jpg',
    URL_ACTIVA:      'https://fastenglish.com.mx/activarcuenta/#/',
    RUTA_ARCHIVOS:   'https://lms.fastenglish.com.mx/',
    RUTA_MANUAL_CERTIFICACION: 'https://lms.fastenglish.com.mx/manuales/certificacion_usuario_FAST.pdf',
    RUTA_EXAMEN_CERTIFICACION: 'https://www.metritests.com/metrica/',
    RUTA_EXCI:       'https://lms.fastenglish.com.mx/exci/exci.php?id=',
    URL_HORARIOS     : 'https://fastenglish.com.mx/public/images/fast_horarios_2023.png',
    URL_PAGOS        : '',
    TOKEN            : 'f7RGvnRCq0G2pW9cJkrY6O:APA91bFHHrPpvE7G9wfYdjDH54boS1Rjy_Aeq-FaD7cdPeUDGbaIIsSlByq4nif23fFKpb-aSoLbIRS_1SM1gVcw5N4430OjmXTDoyASq-AvME4-Oa2rk4ySw0awh5NAA2Nx6Y1-33kJ',
    TOKENDESARROLLO  : 'cMK81le8_UCfvLUtXTBsRE:APA91bGkw0JKJ2Gvy6iUqXyTmDtrKk8Ncpg63byk7EGx6Uj19mIrU3oj2ZWN6oSPntIlQokg_G1gBNtwKQvOJVufDxnSdYXJZ96-LVNudv87e5o95ry4Bh2SPeCO0Nt67hhA41pfX-Zz',
    TOKENARMANDO     : 'd6FwKI_ERaiiI9uQ1pJmjq:APA91bE0vhRYVxoY_ZSvyt49irOKrmgruWVWkO4eCWcaD4xQF4_jqn8FwxbgnphihSq0N6bVzi49SNrGaKWIsLgFpizB3vXbxqgc-mRBpKpu-Ib705cUWKwjKNov1j5ePKtnCIQaJoOm'

  }
}
