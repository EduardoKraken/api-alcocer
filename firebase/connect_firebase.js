const admin = require("firebase-admin");

const serviceAccount = require("./push-notification-7bb5a-firebase-adminsdk-56w94-e070ea394f.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const helperNotificaciones = {

	sendNotification ( notification ) {
		return new Promise(( resolve, reject )=>{
		  admin.messaging().send( notification ).then((response) => {
		  	resolve( 'Successfully sent message:', response )
		    console.log('Successfully sent message:', response);
		  })
		  .catch((error) => {
		    console.log('Error sending message:', error);
		    resolve( 'Error sending message:', error )
		  })
		})
		
	},

	sendPushToTopic(notification) {
		return new Promise(( resolve, reject )=>{
		  admin.messaging().send( notification ).then((response) => {
		  	resolve( 'Successfully sent message:', response )
		    console.log('Successfully sent message:', response);
		  })
		  .catch((error) => {
		    console.log('Error sending message:', error);
		    resolve( 'Error sending message:', error )
		  })
		})
	},

	suscribir( ){
		const registrationTokens = [
		  'cPWSCIypQ3S4rEKyGZHzAU:APA91bF_O8iFOG2HSm5HKFOYOBHqH2WBOjHUQqCzkEzEvR82DHwOGy7ZySZpOCEcE3-ObYgwqCpiWDyHnOxf092u3mdpTuVn_uBoZc9kpsS1sStRfFwxCWgpUNZmz8MFLi56p7NSyNzf',
		  'f2VESgFoSSagdbvLnXXjdV:APA91bG35reJNJ34zcBry4wwj6eKgm-l26EmW6zOo26k-blgSxJdEdYtTcNHoqB-Tc_41rjo0ElsLQw6A33fUDVqJ9R-hoj859Q7pTBJ640pLXn7wsQMuHgLLeuCmhEN81MC80WDvoIn'
		];

		// Subscribe the devices corresponding to the registration tokens to the
		// topic.
		// unsubscribeFromTopic
		admin.messaging().subscribeToTopic(registrationTokens, 'topic').then((response) => {
	    // See the MessagingTopicManagementResponse reference documentation
	    // for the contents of response.
	    console.log('Successfully subscribed to topic:', response);
	  })
	  .catch((error) => {
	    console.log('Error subscribing to topic:', error);
	  });
	}
}

module.exports = helperNotificaciones  

function sendMessage(message) {
  admin.messaging().send(message).then((response) => {
    console.log('Successfully sent message:', response);
  })
  .catch((error) => {
    console.log('Error sending message:', error);
  })
}
