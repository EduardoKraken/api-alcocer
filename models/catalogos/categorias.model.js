const { result }  = require("lodash");
const sql     = require("../db.js");

//const constructor
const categorias = (clases) => {};

categorias.addCategorias = ( u ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO categorias( categoria ) VALUES( ? )`, [ u.categoria ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...u })
    })
  })
}

categorias.updateCategorias = ( u ) => {
	return new Promise(( resolve, reject )=>{
	  sql.query(`UPDATE categorias SET categoria = ?, deleted = ?  WHERE idcategorias = ?`,[ u.categoria, u.deleted, u.idcategorias ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve( u );
	  });
	});
};

categorias.getCategorias = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM categorias WHERE deleted = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


module.exports = categorias;