const { result }  = require("lodash");
const sql     = require("../db.js");

//const constructor
const usuarios = (clases) => {};

usuarios.addUsuarios = ( u ) => {
  console.log( u )
  return new Promise((resolve,reject)=>{
  sql.query(`INSERT INTO usuarios( usuario, password, nombres, apellidos, edad, telefono, fecha_pago, nivel, estatus ) VALUES( ?,MD5(?),?,?,?,?,?,?,? )`,
    [ u.usuario, u.password, u.nombres, u.apellidos, u.edad, u.telefono, u.fecha_pago, u.nivel, u.estatus ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...u })
    })
  })
}

usuarios.updateUsuarios = ( u, id ) => {
	return new Promise(( resolve, reject )=>{
	  sql.query(`UPDATE usuarios SET nombres= ?, apellidos= ?, edad= ?, telefono= ?, fecha_pago= ?, nivel= ?, estatus= ?, deleted = ?
      WHERE idusuarios = ?`,[ u.nombres, u.apellidos, u.edad, u.telefono, u.fecha_pago, u.nivel, u.estatus, u.deleted , id ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve( u );
	  });
	});
};

usuarios.getUsuarios = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT *, CONCAT( nombres, ' ', apellidos ) AS nombre_completo FROM usuarios WHERE deleted = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

usuarios.existeUsuario = ( usuarios ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM usuarios WHERE usuario = ?;`,[ usuarios ],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res[0])
    })
  })
}

module.exports = usuarios;