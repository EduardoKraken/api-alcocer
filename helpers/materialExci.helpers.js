const config     = require("../config/index.js");

const escuela = config.UNIDAD_NEGOCIO


//const constructor
const helperMateialExci = {

	getMaterialExciTodos (  ) {
		const material = [
   		{
   			titulo: 'Material de bienvenida',
   			action: 'mdi-folder-multiple',
   			libre: true,
   			materiales:[
   				{nombre: 'Bienvenida'   , tipo: 'pdf', icon: 'mdi-file-pdf-box theme--light', path: '/1/Bienvenida.pdf'},
   				{nombre: 'Auxiliares'   , tipo: 'mp4', icon: 'mdi-play-box theme--light'    , path: '/1/Auxiliares.mp4'},
   				{nombre: 'Personas'     , tipo: 'mp4', icon: 'mdi-play-box theme--light'    , path: '/1/Pronombres.mp4'},
   				{nombre: 'Recomendación', tipo: 'mp4', icon: 'mdi-play-box theme--light'    , path: '/1/Recomendacionesdelexamen.mp4'},
   				{nombre: 'Verbo to be'  , tipo: 'mp4', icon: 'mdi-play-box theme--light'    , path: '/1/VerboToBe.mp4'},
   			]
   		},
   		{
   			titulo: 'Examen de diagnóstico',
   			action: 'mdi-folder-multiple',
   			libre: true,
   			materiales:[
   				{nombre: 'Examen Diagnóstico', tipo:'examen', icon: 'mdi-book-check-outline', id: escuela == 2 ? 1438 : 3440 }
   			]
   		},
   		{
   			titulo: 'Temas de práctica 1',
   			action: 'mdi-folder-multiple',
   			libre: false,
   			materiales:[
   				{nombre: 'Verbo to be', tipo: 'pdf'      , icon: 'mdi-file-pdf-box theme--light', path: '/3/verbo-to-be.pdf'},
   				// {nombre: 'Verbo to be', tipo: 'ejercicio', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1641 : 3441 },
   				{nombre: 'Pronombres' , tipo: 'pdf'      , icon: 'mdi-file-pdf-box theme--light', path: '/3/pronombres.pdf'},
   				// {nombre: 'Pronombres' , tipo: 'ejercicio', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1642 : 3442 },
   				{nombre: 'Posesivos'  , tipo: 'pdf'      , icon: 'mdi-file-pdf-box theme--light', path: '/3/posesivos.pdf'},
   				// {nombre: 'Posesivos'  , tipo: 'ejercicio', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1643 : 3443 },
   				{nombre: 'Auxiliares' , tipo: 'pdf'      , icon: 'mdi-file-pdf-box theme--light', path: '/3/auxiliares.pdf'},
   				// {nombre: 'Auxiliares' , tipo: 'ejercicio', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1644 : 3444 },
   			]
   		},
   		{
   			titulo: 'Temas de práctica 2',
   			action: 'mdi-folder-multiple',
   			libre: false,
   			materiales:[
   				{nombre: 'Presente'         , tipo: 'pdf'      , icon: 'mdi-file-pdf-box theme--light', path: '/4/Presente.pdf'},
   				// {nombre: 'Presente'         , tipo: 'ejercicio', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1666 : 3445 },
   				{nombre: 'Pasado'           , tipo: 'pdf'      , icon: 'mdi-file-pdf-box theme--light', path: '/4/Pasado.pdf'},
   				// {nombre: 'Pasado'           , tipo: 'ejercicio', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1667 : 3446 },
   				{nombre: 'Futuro'           , tipo: 'pdf'      , icon: 'mdi-file-pdf-box theme--light', path: '/4/Futuro.pdf'},
   				{nombre: 'Presente perfecto', tipo: 'pdf'      , icon: 'mdi-file-pdf-box theme--light', path: '/4/Presente-perfecto.pdf'},
   				// {nombre: 'Presente perfecto', tipo: 'ejercicio', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1648 : 3447 },
   				{nombre: 'Conectores'       , tipo: 'pdf'      , icon: 'mdi-file-pdf-box theme--light', path: '/4/Conectores.pdf'},
   			]
   		},
   		{
   			titulo: ' Temas de práctica 3',
   			action: 'mdi-folder-multiple',
   			libre: false,
   			materiales:[
   				{nombre: 'Gerundios'         , tipo: 'pdf'      , icon: 'mdi-file-pdf-box theme--light', path: '/5/Gerundios.pdf'},
   				{nombre: 'Artículos'         , tipo: 'pdf'      , icon: 'mdi-file-pdf-box theme--light', path: '/5/Articulos.pdf'},
   				{nombre: 'Verbos regulares'  , tipo: 'pdf'      , icon: 'mdi-file-pdf-box theme--light', path: '/5/Verbos-regulares.pdf'},
   				{nombre: 'Verbos irregulares', tipo: 'pdf'      , icon: 'mdi-file-pdf-box theme--light', path: '/5/Verbos-irregulares.pdf'},
   				{nombre: 'Preposiciones'     , tipo: 'pdf'      , icon: 'mdi-file-pdf-box theme--light', path: '/5/Preposiciones.pdf'},
   				// {nombre: 'Preposiciones'     , tipo: 'ejercicio', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1669 : 3448 },
   			]
   		},
   		{
   			titulo: 'Video para practicar LISTENING',
   			action: 'mdi-folder-multiple',
   			libre: false,
   			materiales:[
   				{nombre: 'Video para practicar', tipo: 'mp4' , icon: 'mdi-play-box theme--light',  path: '/1/video_practica.mp4'},
   			]
   		},
   	]
	  return material
	},

	getMaterialClase1 (  ) {
		const material = [
   		{
   			titulo: 'Clase 1',
   			action: 'mdi-folder-multiple',
   			libre: false,
   			materiales:[
   				{nombre: 'Ejercicio 1'                 , tipo: 'ejercicio', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1659 : 3449 },
   				{nombre: 'Ejercicio 2'                 , tipo: 'ejercicio', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1660 : 3450 },
   				{nombre: 'Ejercicio 3'                 , tipo: 'ejercicio', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1661 : 3451 },
   				{nombre: 'Ejercicio 4'                 , tipo: 'ejercicio', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1662 : 3452 },
   				{nombre: 'Ejercicio 5'                 , tipo: 'ejercicio', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1442 : 3456 },
   				{nombre: 'Tarea (después de clase) '   , tipo: 'ejercicio', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1444 : 3457 },

   			]
   		}
   	]
	  return material
	},

	getMaterialClase2 (  ) {
		const material = [
   		{
   			titulo: 'Clase 2',
   			action: 'mdi-folder-multiple',
   			libre: false,
   			materiales:[
   				// {nombre: 'Ejercicio 1'                 , tipo: 'ejercicio', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1663 : 3453 },
   				// {nombre: 'Ejercicio 2'                 , tipo: 'ejercicio', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1664 : 3454 },
   				// {nombre: 'Ejercicio 3'                 , tipo: 'ejercicio', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1449 : 3458 },
   				{nombre: 'Reglas'                      , tipo: 'pdf'      , icon: 'mdi-file-pdf-box theme--light', path: '/5/Reglas.pdf'},
   			]
   		},
   		{
   			titulo: 'Examen Final',
   			action: 'mdi-folder-multiple',
   			libre: false,
   			materiales:[
   				{nombre: 'Examen Final'                , tipo: 'examen', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1665 : 3455 },
   			]
   		},
   		{
   			titulo: 'Exci de práctica',
   			action: 'mdi-folder-multiple',
   			libre: false,
   			materiales:[
   				{nombre: 'Exci de práctica'                , tipo: 'examen', icon: 'mdi-book-check-outline'       , id: escuela == 2 ? 1671 : 3455 },
   			]
   		}

   		
   	]
	  return material
	},
};


module.exports = helperMateialExci;