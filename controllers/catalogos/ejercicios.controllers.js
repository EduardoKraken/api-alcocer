const ejercicios   = require("../../models/catalogos/ejercicios.model.js");

// Enviar el recibo de pago al alumno
exports.addEjercicio = async(req, res) => {
  try {

    const ejercicio   = await ejercicios.addEjercicio( req.body ).then( response => response )

    res.send({message: 'Exito en el proceso' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateEjercicio= async(req, res) => {
  try {

    const { id } = req.params

    const ejercicio   = await ejercicios.updateEjercicio( req.body, id ).then( response => response )

    res.send( ejercicio );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getEjercicios = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const ejercicio = await ejercicios.getEjercicios( ).then( response=> response ) 
      
    res.send( ejercicio );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};
