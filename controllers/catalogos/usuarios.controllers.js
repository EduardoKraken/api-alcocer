const usuarios   = require("../../models/catalogos/usuarios.model.js");

// Enviar el recibo de pago al alumno
exports.addUsuarios = async(req, res) => {
  try {

    const { usuario } = req.body

    // BUscar primero si el suaurio que se está dando de alta ya existe en el abase de datos
    // si es así, hay que retornar un error de que el usuario ya existe
    const existeUsuario = await usuarios.existeUsuario( usuario ).then(response => response) 

    if( existeUsuario ){
      return res.status( 500 ).send({ message: 'El usuario que intentas agregar ya existe.' })
    }

    const usuarioAgregado  = await usuarios.addUsuarios( req.body ).then( response => response )

    res.send({message: 'Exito en el proceso' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateUsuarios= async(req, res) => {
  try {

    const { id } = req.params

    const usuario   = await usuarios.updateUsuarios( req.body, id ).then( response => response )

    res.send( usuario );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getUsuarios = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const usuario = await usuarios.getUsuarios( ).then( response=> response ) 
      
    res.send( usuario );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};
