const categorias   = require("../../models/catalogos/categorias.model.js");

// Enviar el recibo de pago al alumno
exports.addCategorias = async(req, res) => {
  try {

    const categoria   = await categorias.addCategorias( req.body ).then( response => response )

    res.send({message: 'Exito en el proceso' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateCategorias= async(req, res) => {
  try {

    const { id } = req.params

    const categoria   = await categorias.updateCategorias( req.body, id ).then( response => response )

    res.send( categoria );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getCategorias = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const categoria = await categorias.getCategorias( ).then( response=> response ) 
      
    res.send( categoria );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};
