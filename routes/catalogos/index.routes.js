module.exports = app => {
    require("./categorias.routes")(app)
    require("./ejercicios.routes")(app)
    require("./usuarios.routes")(app)
  };