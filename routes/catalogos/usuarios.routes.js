module.exports = app => {
  const usuarios = require('../../controllers/catalogos/usuarios.controllers') // --> ADDED THIS
  app.post("/usuarios.add"      ,  usuarios.addUsuarios); 
  app.put("/usuarios.update/:id",  usuarios.updateUsuarios); 
  app.get("/usuarios.list"      ,  usuarios.getUsuarios); 
};